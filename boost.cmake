####################################################
# Boost

message( STATUS "\n--- Boost ----" )

# Force Boost.Thread V4 use for all projects, including dependencies.
set( NETRUSH_BOOST_COMPILE_DEFINITIONS
    -DBOOST_THREAD_VERSION=4
    -DBOOST_RESULT_OF_USE_DECLTYPE
)

set( NETRUSH_BOOST_COMPONENTS
    date_time
    filesystem
    system
    chrono
    thread
    program_options
    log
    log_setup
)

if( WIN32 )
    # On Windows: check that the right boost binaries are set before continuing
    if( NOT DEFINED BOOST_LIBRARYDIR OR BOOST_LIBRARYDIR STREQUAL "BOOST_LIBRARYDIR-NOT-SET" )
        set( BOOST_LIBRARYDIR "BOOST_LIBRARYDIR-NOT-SET" CACHE PATH "Location of the Boost library binaries" FORCE )
        message( FATAL_ERROR "BOOST_LIBRARYDIR is not set (we need the 64bit version). Before continuing, please set it to the correct binary path (depending on if you want to link with 32 or 64bit version)." )
    endif()

endif()

set( Boost_USE_STATIC_LIBS        ON )
set( Boost_USE_MULTITHREADED      ON )
set( Boost_USE_STATIC_RUNTIME    OFF )

find_package( Boost 1.57.0 REQUIRED COMPONENTS ${NETRUSH_BOOST_COMPONENTS} )

if( NOT Boost_FOUND )
    message( FATAL_ERROR "Boost libraries NOT FOUND! NOTHING CAN HAPPEN WITHOUT BOOST!!!!!!!!" )
endif()

foreach( boost_library_name ${NETRUSH_BOOST_COMPONENTS} )
    string( TOUPPER ${boost_library_name} BOOST_LIBRARY_NAME )

    set( imported_target_name "boost_${BOOST_LIBRARY_NAME}" )

    add_library( ${imported_target_name} IMPORTED STATIC GLOBAL)
    set_property( TARGET ${imported_target_name}
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES
        ${Boost_INCLUDE_DIRS}
    )
    set_property( TARGET ${imported_target_name}
        PROPERTY INTERFACE_COMPILE_DEFINITIONS
        ${NETRUSH_BOOST_COMPILE_DEFINITIONS}
    )

endforeach()
