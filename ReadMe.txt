
NetRush Dependencies
====================

Provides automatic gathering, build and install of
NetRush dependencies, ready to be used in the project.

This is designed to help do the following as fast as possible:

 - change dependencies versions;
 - change dependency libraries;
 - change dependency build configs;


How To Do Anything
------------------

1. Use CMake to build this project.
2. Build all (make sure you build Debug, Release and the rest)
3. If nothing got wrong you are set.

The resulting install directory will be located in the
"netrush-deps" directory in the root build directory.

TODO:
-----

 - complete installation;
 - don't always download, see UPDATE_DISCONNECTED;
 - find a way to not rebuild tbb each time there is a change in the repo (maybe the split helps);
 - split targets into several targets, one for download, etc.
 - port to linux;
 - port to macosx;

