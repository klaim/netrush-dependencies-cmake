
####################################################
# Google Test
message( STATUS "--- Google test ----" )

ExternalProject_Add( gtest
    URL https://googletest.googlecode.com/files/gtest-1.7.0.zip
    URL_HASH SHA1=f85f6d2481e2c6c4a18539e391aa4ea8ab0394af

    CMAKE_CACHE_ARGS
        -Dgtest_force_shared_crt:bool=TRUE
        
    INSTALL_COMMAND ""
)



