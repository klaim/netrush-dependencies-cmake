
####################################################
# Threading Building Blocks
message( STATUS "--- Threading Building Blocks ----" )

# Where the library files will be located in the end:
set( _tbb_install_dir ${NETRUSH_DEPENDENCIES_DIR}/tbb )
set( TBB_ROOT ${_tbb_install_dir} )
set( TBB_SOURCE_INCLUDE_DIR ${CMAKE_BINARY_DIR}/tbb-prefix/src/tbb/include )
set( TBB_INCLUDE_DIR ${_tbb_install_dir}/include )
set( TBB_LIBRARY_DIR ${_tbb_install_dir}/lib )
set( TBB_BIN_DIR ${_tbb_install_dir}/bin )

set( _tbb_install_command ${CMAKE_COMMAND} -E echo Installing TBB... )
set( _tbb_libraries_names tbb tbbmalloc tbbmalloc_proxy )
set( _tbb_debug_suffix _debug )

if( MSVC )

    if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
        set( _tbb_architecture x64 )
        set( _tbb_build_dir_prefix intel64 )
    else()
        set( _tbb_architecture Win32 )
        set( _tbb_build_dir_prefix ia32 )
    endif()

    set( _tbb_vs_solution ${CMAKE_BINARY_DIR}/tbb-prefix/src/tbb/build/vs2010/makefile.sln )

    set( _tbb_configure_command devenv "${_tbb_vs_solution}" /upgrade ) # Make sure to upgrade to the Visual Studio version we want.

    set( _tbb_build_solution_command msbuild "${_tbb_vs_solution}" /maxcpucount /property:Platform=${_tbb_architecture} /property:Configuration= )
    set( _tbb_build_command
           ${_tbb_build_solution_command}Debug
        && ${_tbb_build_solution_command}Release
        #&& ${_tbb_build_solution_command}Debug-MT # TODO: needed?
        #&& ${_tbb_build_solution_command}Release-MT
    )

    set( _tbb_build_dir ${CMAKE_BINARY_DIR}/tbb-prefix/src/tbb/build/vs2010/${_tbb_build_dir_prefix} )

    set( _tbb_debug_libraries_dir   ${_tbb_build_dir}/Debug )
    set( _tbb_debug_binaries_dir    ${_tbb_build_dir}/Debug )

    set( _tbb_release_libraries_dir ${_tbb_build_dir}/Release )
    set( _tbb_release_binaries_dir  ${_tbb_build_dir}/Release )

    list( APPEND TBB_LIB_SEARCH_PATH ${_tbb_release_libraries_dir} )
    list( APPEND TBB_LIB_SEARCH_PATH ${_tbb_release_binaries_dir}  )
    list( APPEND TBB_LIB_SEARCH_PATH ${_tbb_debug_libraries_dir}   )
    list( APPEND TBB_LIB_SEARCH_PATH ${_tbb_debug_binaries_dir}    )

    foreach( _tbb_lib_name ${_tbb_libraries_names} )
        set( _tbb_lib_name_debug ${_tbb_lib_name}${_tbb_debug_suffix} )
        set( _tbb_install_command ${_tbb_install_command}
            && ${CMAKE_COMMAND} -E copy_if_different ${_tbb_debug_libraries_dir}/${_tbb_lib_name_debug}.lib ${TBB_LIBRARY_DIR}/${_tbb_lib_name_debug}.lib
            && ${CMAKE_COMMAND} -E copy_if_different ${_tbb_debug_binaries_dir}/${_tbb_lib_name_debug}.dll  ${TBB_BIN_DIR}/${_tbb_lib_name_debug}.dll
            && ${CMAKE_COMMAND} -E copy_if_different ${_tbb_release_libraries_dir}/${_tbb_lib_name}.lib     ${TBB_LIBRARY_DIR}/${_tbb_lib_name}.lib
            && ${CMAKE_COMMAND} -E copy_if_different ${_tbb_release_binaries_dir}/${_tbb_lib_name}.dll      ${TBB_BIN_DIR}/${_tbb_lib_name}.dll
        )
        set( _tbb_binaries_release  ${_tbb_binaries_release}  ${_tbb_lib_name}.dll       )
        set( _tbb_binaries_debug    ${_tbb_binaries_debug}    ${_tbb_lib_name_debug}.dll )
        set( _tbb_libraries_release ${_tbb_libraries_release} ${_tbb_lib_name}.lib       )
        set( _tbb_libraries_debug   ${_tbb_libraries_debug}   ${_tbb_lib_name_debug}.lib )
    endforeach()

elseif( UNIX )

    set( _tbb_configure_command "" )
    set( _tbb_build_command make )

    set( _tbb_build_dir ${CMAKE_BINARY_DIR}/tbb-prefix/src/tbb/build )

    # tbb's makefile put output files in dirs like:
    # linux_intel64_gcc_cc4.9.2_libc2.19_kernel3.16.0_release
    # it would be overly complicated and unportable to determine those things
    # by cmake, so we use wildcards
    # we can't use file(GLOB) since these files are generated at build-time and
    # file(GLOB) works at configure-time
    set( _tbb_debug_libraries_dir   ${_tbb_build_dir}/linux_*_debug )
    set( _tbb_release_libraries_dir   ${_tbb_build_dir}/linux_*_release )

    foreach( _tbb_lib_name ${_tbb_libraries_names} )
        set( _tbb_lib_name_debug ${_tbb_lib_name}${_tbb_debug_suffix} )
        # since we use wildcards (see above) and INSTALL_COMMAND does not
        # support them, we must use sh to interpret those
        set( _tbb_install_command ${_tbb_install_command}
            && sh -c "${CMAKE_COMMAND} -E copy_if_different ${_tbb_debug_libraries_dir}/lib${_tbb_lib_name_debug}.so.2 ${TBB_LIBRARY_DIR}/lib${_tbb_lib_name_debug}.so.2"
            && sh -c "${CMAKE_COMMAND} -E copy_if_different ${_tbb_debug_libraries_dir}/lib${_tbb_lib_name_debug}.so ${TBB_LIBRARY_DIR}/lib${_tbb_lib_name_debug}.so"
            && sh -c "${CMAKE_COMMAND} -E copy_if_different ${_tbb_release_libraries_dir}/lib${_tbb_lib_name}.so.2 ${TBB_LIBRARY_DIR}/lib${_tbb_lib_name}.so.2"
            && sh -c "${CMAKE_COMMAND} -E copy_if_different ${_tbb_release_libraries_dir}/lib${_tbb_lib_name}.so ${TBB_LIBRARY_DIR}/lib${_tbb_lib_name}.so"
        )
    endforeach()

else()
    message( FATAL_ERROR "TBB build setup not yet available in this context. WRITE IT NOW!!!" )
endif()

set( _tbb_install_command ${_tbb_install_command}
    && ${CMAKE_COMMAND} -E copy_directory ${TBB_SOURCE_INCLUDE_DIR} ${TBB_INCLUDE_DIR}
)

ExternalProject_Add( tbb
    URL https://www.threadingbuildingblocks.org/sites/default/files/software_releases/source/tbb43_20150611oss_src.tgz
    URL_MD5 bb144ec868c53244ea6be11921d86f03
    BUILD_IN_SOURCE 1

    CONFIGURE_COMMAND "${_tbb_configure_command}"
    BUILD_COMMAND "${_tbb_build_command}"

    INSTALL_COMMAND "${_tbb_install_command}"
)
