
####################################################
# Ogre Dependencies
message( STATUS "--- Ogre Dependencies ----" )

set( _ogre_deps_install_dir ${NETRUSH_DEPENDENCIES_DIR}/ogredeps )

ExternalProject_Add( ogre_dependencies
    HG_REPOSITORY https://bitbucket.org/cabalistic/ogredeps
    HG_TAG 09db5f9d11cff1bf12371896cf7ad946d616994d

    INSTALL_DIR "${_ogre_deps_install_dir}"

    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:path=${_ogre_deps_install_dir}

    CMAKE_CACHE_ARGS

        # Use Boost
        -DBoost_INCLUDE_DIR:path=${Boost_INCLUDE_DIR}
        -DBOOST_ROOT:path=${BOOST_ROOT}
        -DBOOST_LIBRARYDIR:path=${BOOST_LIBRARYDIR}

        # we will not need all dependencies
        -DOGREDEPS_BUILD_CG:bool=FALSE
        -DOGREDEPS_BUILD_FREEIMAGE:bool=TRUE
        -DOGREDEPS_BUILD_FREETYPE:bool=TRUE
        -DOGREDEPS_BUILD_OIS:bool=TRUE
        -DOGREDEPS_BUILD_ZLIB:bool=TRUE
        -DOGREDEPS_BUILD_ZZIPLIB:bool=TRUE
        -DOGREDEPS_INSTALL_DEV:bool=TRUE

        # force OIS to build in static mode
        -DOIS_FORCE_STATIC_BUILD:bool=TRUE # TODO: requires a missing patch to work
)

set( OGRE_DEPENDENCIES_DIR ${_ogre_deps_install_dir} )
set( OIS_INCLUDE_DIRS ${OGRE_DEPENDENCIES_DIR}/include/OIS )
