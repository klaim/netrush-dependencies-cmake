
####################################################
# Google Protobuf
message( STATUS "--- Google Protobuf ----" )

set( _protobuf_root_cmakelists " add_subdirectory( ./cmake ) " )

ExternalProject_Add( protobuf
    GIT_REPOSITORY https://github.com/google/protobuf.git
    GIT_TAG master

    PATCH_COMMAND ${CMAKE_COMMAND} -E echo "${_protobuf_root_cmakelists}" > CMakeLists.txt

    CMAKE_CACHE_ARGS
        -DBUILD_TESTING:bool=FALSE

    INSTALL_COMMAND ""
)
