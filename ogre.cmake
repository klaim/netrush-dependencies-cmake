
####################################################
# Ogre
message( STATUS "--- Ogre ----" )

if(NOT OGRE_DEPENDENCIES_DIR)
    message( FATAL_ERROR "No Ogre dependencies install path NOT SET but is REQUIRED." )
endif()

set( _ogre_install_dir ${NETRUSH_DEPENDENCIES_DIR}/ogre )

ExternalProject_Add( ogre
    HG_REPOSITORY https://bitbucket.org/sinbad/ogre
    HG_TAG default

    INSTALL_DIR ${_ogre_install_dir}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:path=${_ogre_install_dir}

    CMAKE_CACHE_ARGS
        # use our previously built dependencies
        -DOGRE_DEPENDENCIES_DIR:path=${OGRE_DEPENDENCIES_DIR}
        -DOGRE_INSTALL_DEPENDENCIES:bool=FALSE
        -DOGRE_COPY_DEPENDENCIES:bool=FALSE

        # For threading, TBB should be used
        -DOGRE_CONFIG_THREAD_PROVIDER:string=tbb # not sure this works
        -DTBB_ROOT:path=${TBB_ROOT}
        -DTBB_INCLUDE_DIR:path=${TBB_INCLUDE_DIR}
        -DTBB_LIB_SEARCH_PATH:string=${TBB_LIB_SEARCH_PATH}

        # Use Boost
        -DBoost_INCLUDE_DIR:path=${Boost_INCLUDE_DIR}
        -DBOOST_ROOT:path=${BOOST_ROOT}
        -DBOOST_LIBRARYDIR:path=${BOOST_LIBRARYDIR}

        # we'll use OpenGl only
        -DOGRE_BUILD_RENDERSYSTEM_GL:bool=TRUE
        -DOGRE_BUILD_RENDERSYSTEM_D3D9:bool=FALSE
        -DOGRE_BUILD_RENDERSYSTEM_D3D11:bool=FALSE

        # we use    a limited set of plugin
        -DOGRE_BUILD_PLUGIN_OCTREE:bool=TRUE
        -DOGRE_BUILD_PLUGIN_PCZ:bool=TRUE
        -DOGRE_BUILD_COMPONENT_RTSHADERSYSTEM:bool=TRUE
        -DOGRE_BUILD_RTSHADERSYSTEM_CORE_SHADERS:bool=TRUE
        -DOGRE_BUILD_RTSHADERSYSTEM_EXT_SHADERS:bool=TRUE

        -DOGRE_BUILD_PLUGIN_PFX:bool=FALSE
        -DOGRE_BUILD_PLUGIN_BSP:bool=FALSE
        -DOGRE_BUILD_COMPONENT_PAGING:bool=FALSE
        -DOGRE_BUILD_COMPONENT_TERRAIN:bool=FALSE
        -DOGRE_BUILD_COMPONENT_PROPERTY:bool=FALSE
        -DOGRE_BUILD_COMPONENT_OVERLAY:bool=FALSE
        -DOGRE_BUILD_COMPONENT_VOLUME:bool=FALSE
        -DOGRE_BUILD_PLUGIN_CG:bool=FALSE

        # we don't need samples nor tests nor docs
        -DOGRE_BUILD_SAMPLES:bool=FALSE
        -DOGRE_BUILD_TESTS:bool=FALSE
        -DOGRE_BUILD_DOCS:bool=FALSE
        -DOGRE_INSTALL_DOCS:bool=FALSE

        # To accelerate builds, we setup Unity Builds for Ogre, at least in development.
        -DOGRE_UNITY_BUILD:bool=TRUE


        # We deactivate threading in Ogre for now, we only need it to do what we ask.
        # -DOGRE_CONFIG_THREADS:string="0"

)

# Make sure we don't build Ogre before it's dependencies...
ExternalProject_Add_StepDependencies( ogre configure
    ogre_dependencies
    tbb
)

set( OGRE_HOME ${_ogre_install_dir} )



