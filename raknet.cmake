
####################################################
# RakNet
message( STATUS "--- RakNet ----" )

ExternalProject_Add( raknet
    GIT_REPOSITORY https://github.com/OculusVR/RakNet.git
    GIT_TAG master

    CMAKE_CACHE_ARGS
        # we don't want the samples/examples
        -DRAKNET_GENERATE_SAMPLES:bool=FALSE

        # we're using the static version
        -DRAKNET_ENABLE_DLL:bool=FALSE
        -DRAKNET_ENABLE_STATIC:bool=TRUE
        -DRAKNET_GENERATE_INCLUDE_ONLY_DIR:bool=FALSE

    CMAKE_ARGS
        # in debug modes we want to ignore debug breaks time jumps
        -DCMAKE_CXX_FLAGS_DEBUG:STRING="${CMAKE_CXX_FLAGS_DEBUG}  -DGET_TIME_SPIKE_LIMIT=1000000"
        -DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="${CMAKE_CXX_FLAGS_RELWITHDEBINFO}  -DGET_TIME_SPIKE_LIMIT=1000000"

    INSTALL_COMMAND ""
)

