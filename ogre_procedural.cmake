
####################################################
# Ogre Procedural
message( STATUS "--- Ogre Procedural ----" )

set( _ogre_procedural_install_dir ${NETRUSH_DEPENDENCIES_DIR}/ogre_procedural )


ExternalProject_Add( ogre_procedural
    HG_REPOSITORY https://bitbucket.org/transporter/ogre-procedural
    HG_TAG default
    PATCH_COMMAND hg import ${CMAKE_CURRENT_SOURCE_DIR}/ogre-procedural-boost-tbb-patch.diff

    INSTALL_DIR ${_ogre_procedural_install_dir}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:path=${_ogre_procedural_install_dir}

    CMAKE_CACHE_ARGS
        # dependencies
        -DOGRE_HOME:path=${OGRE_HOME}
        -DOGRE_DEPENDENCIES_DIR:path=${OGRE_DEPENDENCIES_DIR}

        # Use Boost and TBB
        -DBoost_INCLUDE_DIR:path=${Boost_INCLUDE_DIR}
        -DBOOST_ROOT:path=${BOOST_ROOT}
        -DBOOST_LIBRARYDIR:path=${BOOST_LIBRARYDIR}
        -DTBB_ROOT:path=${TBB_ROOT}
        -DTBB_INCLUDE_DIR:path=${TBB_INCLUDE_DIR}
        -DTBB_LIB_SEARCH_PATH:string=${TBB_LIB_SEARCH_PATH}

        # we don't want the samples/examples/tests
        -DOgreProcedural_BUILD_SAMPLES:bool=FALSE
        -DOgreProcedural_BUILD_TESTS:bool=FALSE
        -DOgreProcedural_BUILD_LUA_TESTS:bool=FALSE
        -DOgreProcedural_BUILD_DOCS:bool=FALSE
        -DOgreProcedural_INSTALL_DOCS:bool=FALSE
        -DOgreProcedural_BUILD_SAMPLES:bool=FALSE
        -DOgreProcedural_STATIC:bool=FALSE
        -DOgreProcedural_ILLUSTRATIONS:bool=FALSE

        # fast compilations
        -DOgreProcedural_UNITY_BUILD:bool=TRUE
)

ExternalProject_Add_StepDependencies( ogre_procedural configure
    ogre
    tbb
)

